import React, {useState, useEffect} from 'react'
import {Text, View, Dimensions, StyleSheet, Image, ScrollView , TouchableOpacity } from 'react-native';
import { getBookings, getBookingDetails } from '../api/api';
import AwesomeAlert from 'react-native-awesome-alerts';
import BookingCard from '../components/BookingCard';

const {width} = Dimensions.get('window');

const HomeScreen = () => {

    const [data, setData] = useState([]);

    const [showBookingModal, setShowBookingModal] = useState(false);
    const [bookingDetails, setBookingDetails] = useState({});

    const [buttonText, setButtonText] = useState('');

    useEffect(() => {
        getBookings()
        .then(response => {
            //console.log(response.bookings[0].id)
            setData(response.bookings)
        })
        .catch(error => {
            console.log(error)
        })
    }, [])

    const getBookingDetailsFunc = (id) => {
        console.log('getBookingDetailsFunc');

        getBookingDetails(id)
        .then(response => {
            console.log(response)
            setBookingDetails(response.BookingsRevenueDetails)
        })
        .catch(error => {
            console.log(error)
        })
    }


    const rejectBooking = (id) => {
        console.log('Reject ' + id);
        setButtonText('Reject Booking')
        getBookingDetailsFunc(id)
        setTimeout(() => {
             setShowBookingModal(true)
        }, 0);
    }

    const acceptBooking = (id) => {
        console.log('Accept ' + id);
        setButtonText('Accept Booking')
        getBookingDetailsFunc(id)
        setTimeout(() => {
             setShowBookingModal(true)
        }, 0);
    }


    const showModal = () => {
        setShowBookingModal(true)
    };
    
    const hideModal = () => {
        setShowBookingModal(false)
    };


    const getServicePrice = (withoutTax, giftValue, taxPaid) => {
        return parseInt(withoutTax) + parseInt(giftValue) + parseInt(taxPaid);
    }
    

    const customView = () => {
        return(
            <View style={styles.center} >
               <View style={styles.header} >
                    <Text  style={styles.bookingDetails} >Booking Details</Text>
                    <TouchableOpacity onPress={() => setShowBookingModal(false)} >
                        <Image source={require('../../assets/cancel.png')} style={styles.cross} />
                    </TouchableOpacity>
                </View>
               
               <View style={styles.divider} />

                <View style={styles.center} >

                    <View style={styles.bookingType} >
                        <Text style={styles.bookingType} >Booking Type</Text>
                    </View>


                    <View style={styles.statusContainer} >
                        <Text style={styles.content} >{bookingDetails.BookingType}</Text>
                    </View>

                    <View style={styles.detailsContainer} >
                        <View style={styles.details} >
                            <Text style={styles.title} >Searvice Price</Text>
                            <Text style={styles.content} >SAR {getServicePrice(bookingDetails.priceWithoutTax, bookingDetails.giftcardValue, bookingDetails.taxPaid,)}</Text>
                        </View>
                        <View style={styles.details} >
                            <Text style={styles.title} >Booking Confimartion Fee</Text>
                            <Text style={styles.content} >SAR {bookingDetails.storePlatformFee}</Text>
                        </View>
                        <View style={styles.details} >
                            <Text style={styles.title} >Revenue Earned</Text>
                            <Text style={styles.content} >SAR {bookingDetails.revenueEarned}</Text>
                        </View>
                    </View>
                    
                    <View style={styles.center} >
                        <Text style={styles.description} >This is a prepaid booking and hence no extra amount will be deducted from your wallet</Text>
                    </View>

                </View>

                <View style={styles.divider} />

            </View>
        )
    };
    
    

    return (
        <ScrollView>
           <BookingCard 
            data={data}
            acceptBooking={(id) => acceptBooking(id)}
            rejectBooking={(id) => rejectBooking(id)}
            />


            <AwesomeAlert
                show={showBookingModal}
                showProgress={false}
                closeOnTouchOutside={true}
                closeOnHardwareBackPress={false}
                showCancelButton={true}
                showConfirmButton={true}
                cancelText="Cancel"
                cancelButtonTextStyle={{color: '#02adfd'}}
                cancelButtonStyle={{borderColor: '#02adfd', borderWidth: 1, backgroundColor: 'white'}}
                confirmText={buttonText}
                confirmButtonColor="#02adfd"
                customView={customView()}
                onCancelPressed={() => {
                    hideModal();
                }}
                onConfirmPressed={() => {
                    hideModal();
                }}
            />
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    header: {
        width: width-100,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    center: {
        alignItems: 'center',
        width: width-100,
        //backgroundColor: 'red'
    },
    divider: {
        height: 1,
        width: width-100,
        marginTop: 5,
        marginBottom: 5,
        backgroundColor: 'gray'
    },
    statusContainer: {
        padding: 8,
        marginBottom: 5,
        marginTop: 10,
        backgroundColor: '#ffd64e',
        borderRadius: 4,
    },
    detailsContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        width: width-100,
        justifyContent: 'space-between',
    },
    title: {
        fontSize: 14,
        color: 'black',
        textAlign: 'center',
        fontWeight: 'bold'
    },
    content: {
        fontSize: 12,
        color: 'black',
    },
    details: {
        width: 70,
        alignItems: 'center',
    },
    description: {
        marginTop: 10,
        marginBottom: 10,
        fontSize: 12,
        color: 'black',
        textAlign: 'center',
        fontWeight: 'bold'
    },
    bookingType: {
        marginTop: 5,
        fontSize: 24,
        color: 'black',
        textAlign: 'center',
        fontWeight: 'bold'
    },
    bookingDetails: {
        fontSize: 18,
        color: '#02adfd',
        textAlign: 'center',
        fontWeight: 'bold'
    },
    cross: {
        height: 18,
        width: 18,
        resizeMode: 'contain'
    }
})

export default HomeScreen