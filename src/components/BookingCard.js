import React, {useState, useEffect} from 'react'
import {Text, View, Dimensions, StyleSheet, FlatList } from 'react-native';
import { getBookingDetails } from '../api/api';

import MenuOptions from '../components/MenuOptions';

import Moment from 'moment'

const {width} = Dimensions.get('window');

const BookingCard = (props) => {

    const { data, acceptBooking, rejectBooking } = props;



    return (

        <View style={styles.container} >
            <FlatList
                data={data}
                keyExtractor={(item) => item.id}
                renderItem={({item, index}) => {
                    return(
                        <View style={styles.card} >
                            <View style={styles.menuContainer} >
                                <MenuOptions
                                    acceptBooking={() =>  acceptBooking(item.id)}
                                    rejectBooking={() => rejectBooking(item.id)}
                                />
                            </View>
                            <View style={styles.itemContainer} >
                                <View style={styles.titleContainer} >
                                    <Text style={styles.title} >BOOKING</Text>
                                </View>
                                <View style={styles.contentContainer} >
                                    <Text style={[styles.content, {color: '#98c6d3'}]} >{item.bookingId}</Text>
                                </View>
                            </View>

                            <View style={styles.itemContainer} >
                                <View style={styles.titleContainer} >
                                    <Text style={styles.title} >SERVICE NAME</Text>
                                </View>
                                <View style={styles.contentContainer} >
                                    <Text style={styles.content} >{item.Variation.Service.primaryServiceName}</Text>
                                </View>
                            </View>

                            <View style={styles.itemContainer} >
                                <View style={styles.titleContainer} >
                                    <Text style={styles.title} >DATE & TIME</Text>
                                </View>
                                <View style={styles.contentContainer} >
                                    <Text style={styles.content} > {Moment(item.BookingTime).format("DD MMM HH:ss A")} </Text>
                                </View>
                            </View>

                            <View style={styles.itemContainer} >
                                <View style={styles.titleContainer} >
                                    <Text style={styles.title} >CUSTOMER</Text>
                                </View>
                                <View style={styles.contentContainer} >
                                    <Text style={styles.content} >{item.UserMain.userName}</Text>
                                </View>
                            </View>

                            <View style={styles.itemContainer} >
                                <View style={styles.titleContainer} >
                                    <Text style={styles.title} >EMAIL</Text>
                                </View>
                                <View style={styles.contentContainer} >
                                    <Text style={styles.content} >{item.UserMain.email}</Text>
                                </View>
                            </View>

                            <View style={styles.itemContainer} >
                                <View style={styles.titleContainer} >
                                    <Text style={styles.title} >PHONE</Text>
                                </View>
                                <View style={styles.contentContainer} >
                                    <Text style={styles.content} >{item.UserMain.phoneNumber}</Text>
                                </View>
                            </View>

                            <View style={styles.itemContainer} >
                                <View style={styles.titleContainer} >
                                    <Text style={styles.title} >SP+FREE</Text>
                                </View>
                                <View style={styles.contentContainer} >
                                    <Text style={[styles.content, {color: 'red'}]} >{'\u0024'} {item.revenueEarned}</Text>
                                </View>
                            </View>

                            <View style={styles.itemContainer} >
                                <View style={styles.titleContainer} >
                                    <Text style={styles.title} >REVENUE</Text>
                                </View>
                                <View style={styles.contentContainer} >
                                    <Text style={[styles.content, {color: 'red'}]} >{'\u0024'} {item.revenueEarned}</Text>
                                </View>
                            </View>

                            <View style={styles.confimationContainer} >
                                <Text style={styles.confirmText} >{item.BookingStatus.title}</Text>
                            </View>
                        </View>
                    )
                }}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        margin: 15,
       
    },
    card: {
        padding: 15,
        backgroundColor: 'white',
        marginBottom: 20,
        borderRadius: 10
    },
    menuContainer: {
        alignItems: 'flex-end'
    },
    itemContainer: {
        flexDirection: 'row',
        height: 40,
        alignItems: 'center'
    },
    confimationContainer: {
        flex: 1,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffe1e3',
        borderRadius: 10,
    },
    confirmText: {
        color: 'black',
        fontSize: 14,
    },
    titleContainer: {
        width: width*0.35,
    },
    contentContainer: {
        flex: 1,
    },
    title: {
        fontSize: 14,
        color: 'black',
        fontWeight: 'bold'
    },
    content: {
        fontSize: 14,
        color: 'black',
    }
})

export default BookingCard