import React, { Component } from 'react';
import { Text, View, Image, StyleSheet, TouchableOpacity } from 'react-native';

import OptionsMenu from "react-native-option-menu";


const MenuOptions = (props) => {

    const { acceptBooking, rejectBooking } = props;
    return (
        <View>
            <OptionsMenu
                customButton={ 
                <View>
                    <Image source={require('../../assets/dots.png')} style={styles.menuImg} />
                </View>
                }  
                destructiveIndex={1}
                options={[
                    "Accept",
                    "Reject", 
                    "Cancel"
                ]}
                actions={[
                    acceptBooking, rejectBooking
                ]}
            />
        </View>
    )
}

export default MenuOptions


const styles = StyleSheet.create({
    icon: {
        height: 30,
        width: 30,
        marginRight: 10,
        resizeMode: 'contain'
    },
    bottomText: {
        fontSize: 12,
        color: 'gray'
    },
    claps: {
        height: 16,
        width: 16,
        marginRight: 5,
        resizeMode: 'contain'
    },
    menuImg: {
        height: 24,
        width: 24,
        resizeMode: 'contain'
    }
})