const baseUrl = 'http://185.252.234.162:6001/api_v1/bookings/store/'


export function getBookings() {
    return(
        fetch(
            baseUrl + 'getStoreBookings/',
            {
                method: 'GET',
                headers: {
                    'Authorization': 'authToken',
                    'Content-Type': 'application/json',
                    'accesstoken': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsInVzZXJOYW1lIjoiVmlrYXMiLCJlbWFpbCI6InZpa2FzMTk5NUBnbWFpbC5jb20iLCJwaG9uZU51bWJlciI6Ijk5ODg0NDIyMzMiLCJ1c2VyVHlwZSI6Miwic3RvcmUiOjEsInRva2VuVHlwZSI6IlN0b3JlIiwiaWF0IjoxNjQ5MjQwMTY4fQ.tyacJ8wILpLFuGbyV_my6f7bG20_IYkXSm_soL3kw6A',
                },
            }
        )
        .then(res => res.json())
    );
}


export function getBookingDetails(id) {
    return(
        fetch(
            baseUrl + 'getBookingsRevenueDetails?bookingId=' + id,
            {
                method: 'GET',
                headers: {
                    'Authorization': 'authToken',
                    'Content-Type': 'application/json',
                    'accesstoken': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsInVzZXJOYW1lIjoiVmlrYXMiLCJlbWFpbCI6InZpa2FzMTk5NUBnbWFpbC5jb20iLCJwaG9uZU51bWJlciI6Ijk5ODg0NDIyMzMiLCJ1c2VyVHlwZSI6Miwic3RvcmUiOjEsInRva2VuVHlwZSI6IlN0b3JlIiwiaWF0IjoxNjQ5MjQwMTY4fQ.tyacJ8wILpLFuGbyV_my6f7bG20_IYkXSm_soL3kw6A',
                },
            }
        )
        .then(res => res.json())
    );
}
